package com.afchealth.afchealthtest.model;

public class DataModel {
  private String url;
  private String title;
  private int width;
  private int height;
  private String upload_at;

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public int getWidth() {
    return width;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public int getHeight() {
    return height;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public String getUpload_at() {
    return upload_at;
  }

  public void setUpload_at(String upload_at) {
    this.upload_at = upload_at;
  }

  @Override public String toString() {
    return "DataModel{"
        + "url='"
        + url
        + '\''
        + ", title='"
        + title
        + '\''
        + ", width="
        + width
        + ", height="
        + height
        + ", upload_at='"
        + upload_at
        + '\''
        + '}';
  }
}
