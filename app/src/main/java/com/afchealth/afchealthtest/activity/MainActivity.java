package com.afchealth.afchealthtest.activity;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import com.afchealth.afchealthtest.R;
import com.afchealth.afchealthtest.adapter.DataListAdapter;
import com.afchealth.afchealthtest.app.AppController;
import com.afchealth.afchealthtest.model.DataModel;
import com.afchealth.afchealthtest.utils.AppConstants;
import com.afchealth.afchealthtest.utils.CustomDividerItemDecoration;
import com.afchealth.afchealthtest.utils.DataParser;
import com.afchealth.afchealthtest.utils.InternetConnection;
import com.afchealth.afchealthtest.utils.RecyclerTouchListener;
import com.afchealth.afchealthtest.utils.VolleyErrorHelper;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
  private static final String TAG = "MainActivity";
  private RecyclerView recyclerView;
  private DataListAdapter mAdapter;
  private SearchView searchView;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    ActionBar bar = getSupportActionBar();
    if (bar != null) {
      bar.setTitle(R.string.toolbar_title);
    }

    recyclerView = findViewById(R.id.recycler_view);
    mAdapter = new DataListAdapter(null);
    whiteNotificationBar(recyclerView);

    checkForRequest();
  }

  private void checkForRequest() {
    if (InternetConnection.checkConnection(this)) {
      request();
    } else {
      AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
      alertDialog.setTitle("No Internet");
      alertDialog.setMessage(
          "No Internet available right now. Please check your internet connection and try again later");
      alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Retry",
          new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
              dialog.dismiss();
              checkForRequest();
            }
          });
      alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Exit",
          new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
              dialog.dismiss();
              finish();
            }
          });
      alertDialog.show();
    }
  }

  private void request() {
    String tag_req = "tag_req";

    final ProgressDialog pDialog = new ProgressDialog(this);
    pDialog.setMessage("Loading...");
    pDialog.show();

    HashMap<String, String> params = new HashMap<>();
    params.put(AppConstants.KEY_NAME, AppConstants.REQUEST_BODY_NAME);
    params.put(AppConstants.KEY_MOBILE_NO, AppConstants.REQUEST_BODY_MOBILE_NO);
    JSONObject parameters = new JSONObject(params);

    JsonObjectRequest jsonObjectRequest =
        new JsonObjectRequest(Request.Method.POST, AppConstants.URL_MAIN, parameters, new Response.Listener<JSONObject>() {

          @Override public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());
            pDialog.hide();
            DataParser parser = new DataParser(response);
            if (parser.checkStatus() != 200) {
              Toast.makeText(MainActivity.this, "" + parser.parseMessage(parser.checkStatus()),
                  Toast.LENGTH_SHORT).show();
            } else {
              showData(parser.parseDataList());
            }
          }
        }, new Response.ErrorListener() {

          @Override public void onErrorResponse(VolleyError error) {
            VolleyLog.d(TAG, "Error: " + error.getMessage());
            pDialog.hide();
            Toast.makeText(MainActivity.this,
                "" + VolleyErrorHelper.getMessage(error, MainActivity.this), Toast.LENGTH_SHORT)
                .show();
          }
        }) {

          /**
           * Passing some request headers
           */
          @Override public Map<String, String> getHeaders() throws AuthFailureError {
            HashMap<String, String> headers = new HashMap<>();
            headers.put("Content-Type", "application/json");
            headers.put(AppConstants.KEY_APP_AUTH_ID, AppConstants.REQUEST_APP_AUTH_ID);
            headers.put(AppConstants.KEY_APP_TOKEN, AppConstants.REQUEST_APP_TOKEN);
            return headers;
          }
        };
    AppController.getInstance().addToRequestQueue(jsonObjectRequest, tag_req);
  }

  private void showData(final ArrayList<DataModel> data_list) {
    mAdapter.setDataList(data_list);
    recyclerView.setHasFixedSize(true);
    RecyclerView.LayoutManager mLayoutManager =
        new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
    recyclerView.setLayoutManager(mLayoutManager);
    recyclerView.addItemDecoration(
        new CustomDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
    recyclerView.setItemAnimator(new DefaultItemAnimator());
    recyclerView.setAdapter(mAdapter);
    recyclerView.addOnItemTouchListener(
        new RecyclerTouchListener(getApplicationContext(), recyclerView,
            new RecyclerTouchListener.ClickListener() {
              @Override public void onClick(View view, int position) {

              }

              @Override public void onLongClick(View view, int position) {
                DataModel dataModel = data_list.get(position);
                String url = dataModel.getUrl();
                Toast.makeText(MainActivity.this, url, Toast.LENGTH_SHORT).show();
              }
            }));
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_main, menu);
    SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
    searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
    if (searchManager != null) {
      searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
    }
    searchView.setMaxWidth(Integer.MAX_VALUE);

    // listening to search query text change
    searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
      @Override public boolean onQueryTextSubmit(String query) {
        // filter recycler view when query submitted
        mAdapter.getFilter().filter(query);
        return false;
      }

      @Override public boolean onQueryTextChange(String query) {
        // filter recycler view when text is changed
        mAdapter.getFilter().filter(query);
        return false;
      }
    });
    return true;
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_search) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override public void onBackPressed() {
    // close search view on back button pressed
    if (!searchView.isIconified()) {
      searchView.setIconified(true);
      return;
    }
    super.onBackPressed();
  }

  private void whiteNotificationBar(View view) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      int flags = view.getSystemUiVisibility();
      flags |= View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
      view.setSystemUiVisibility(flags);
      getWindow().setStatusBarColor(Color.WHITE);
    }
  }
}
