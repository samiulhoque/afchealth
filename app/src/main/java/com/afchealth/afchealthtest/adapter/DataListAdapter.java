package com.afchealth.afchealthtest.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import com.afchealth.afchealthtest.R;
import com.afchealth.afchealthtest.app.AppController;
import com.afchealth.afchealthtest.model.DataModel;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import java.util.ArrayList;
import java.util.List;

public class DataListAdapter extends RecyclerView.Adapter<DataListAdapter.ViewHolder>
    implements Filterable {

  private ImageLoader imageLoader = AppController.getInstance().getImageLoader();
  private List<DataModel> dataList;
  private List<DataModel> dataListFiltered;

  public DataListAdapter(List<DataModel> dataList) {
    this.dataList = dataList;
    this.dataListFiltered = dataList;
  }

  @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View itemView =
        LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row, parent, false);

    return new ViewHolder(itemView);
  }

  @Override public void onBindViewHolder(ViewHolder holder, int position) {
    if (imageLoader == null) {
      imageLoader = AppController.getInstance().getImageLoader();
    }

    DataModel dataModel = dataListFiltered.get(position);
    holder.text_title.setText(dataModel.getTitle());
    String dimension = (dataModel.getWidth() + " * " + dataModel.getHeight());
    holder.text_dimension.setText(dimension);
    holder.text_date.setText(dataModel.getUpload_at());

    holder.iv_thumb.setImageUrl(dataModel.getUrl(), imageLoader);
  }

  @Override public int getItemCount() {
    return dataListFiltered.size();
  }

  public void setDataList(ArrayList<DataModel> dataList) {
    this.dataList = dataList;
    this.dataListFiltered = dataList;
    notifyDataSetChanged();
  }

  @Override public Filter getFilter() {
    return new Filter() {
      @Override protected FilterResults performFiltering(CharSequence charSequence) {
        String charString = charSequence.toString();
        if (charString.isEmpty()) {
          dataListFiltered = dataList;
        } else {
          List<DataModel> filteredList = new ArrayList<>();
          for (DataModel row : dataList) {
            if (row.getTitle().toLowerCase().contains(charString.toLowerCase())) {
              filteredList.add(row);
            }
          }

          dataListFiltered = filteredList;
        }

        FilterResults filterResults = new FilterResults();
        filterResults.values = dataListFiltered;
        return filterResults;
      }

      @Override
      protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
        dataListFiltered = (ArrayList<DataModel>) filterResults.values;
        notifyDataSetChanged();
      }
    };
  }

  class ViewHolder extends RecyclerView.ViewHolder {
    TextView text_title, text_dimension, text_date;
    NetworkImageView iv_thumb;

    ViewHolder(View view) {
      super(view);
      text_title = view.findViewById(R.id.text_title);
      text_dimension = view.findViewById(R.id.text_dimension);
      text_date = view.findViewById(R.id.text_date);

      iv_thumb = view.findViewById(R.id.iv_thumb);
    }
  }
}