package com.afchealth.afchealthtest.utils;

public class AppConstants {
  public static final String URL_MAIN = "http://103.23.41.189:3000/api/idd/interview/test";
  public static final String REQUEST_METHOD = "POST";
  public static final String REQUEST_APP_TOKEN = "4fb8744b8easjdaslkdasjdjksg7c4f45e6dd";
  public static final String REQUEST_APP_AUTH_ID = "iddl_interview";
  public static final String REQUEST_BODY_NAME = "IDDL";
  public static final String REQUEST_BODY_MOBILE_NO = "01823824732938";
  public static final String KEY_MOBILE_NO = "mobile_no";
  public static final String KEY_NAME = "name";
  public static final String KEY_APP_TOKEN = "APPTOKEN";
  public static final String KEY_APP_AUTH_ID = "APPAUTHID";
}
