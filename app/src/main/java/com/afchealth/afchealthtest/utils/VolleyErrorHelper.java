package com.afchealth.afchealthtest.utils;

import android.content.Context;
import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;

public class VolleyErrorHelper {
  private static final String TAG = "VolleyErrorHelper";

  public static String getMessage(VolleyError error, Context context) {
    if (error instanceof TimeoutError) {
      return "Timeout";
    } else if (isServerProblem(error)) {
      return handleServerError(error, context);
    } else if (isNetworkProblem(error)) {
      return "No Internet";
    } else {
      return "Server Error";
    }
  }

  public static int getErrorCode(VolleyError error) {
    NetworkResponse response = error.networkResponse;
    return response.statusCode;
  }

  private static String handleServerError(Object error, Context context) {
    VolleyError er = (VolleyError) error;
    NetworkResponse response = er.networkResponse;
    if (error instanceof ServerError && response != null) {
      try {
        switch (response.statusCode) {
          case 480:
            Log.e(TAG, "handleServerError: 480: " + ((VolleyError) error).getMessage());
          case 400:
            Log.e(TAG, "handleServerError: 400: " + ((VolleyError) error).getMessage());
          default:
            Log.e(TAG, "handleServerError: timeout error: " + ((VolleyError) error).getMessage());
        }

        String res =
            new String(response.data, HttpHeaderParser.parseCharset(response.headers, "utf-8"));
        JSONObject obj = new JSONObject(res);
        return obj.getString("message");
      } catch (UnsupportedEncodingException | JSONException e) {
        Log.e(TAG, "handleServerError: " + e.getMessage());
      }
    }
    return "Server Error";
  }

  private static boolean isServerProblem(Object error) {
    return (error instanceof ServerError || error instanceof AuthFailureError);
  }

  private static boolean isNetworkProblem(Object error) {
    return (error instanceof NetworkError);
  }
}
