package com.afchealth.afchealthtest.utils;

import android.util.Log;
import com.afchealth.afchealthtest.model.DataModel;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DataParser {
  private static final String TAG = "DataParser";
  private JSONObject jsonObject;

  public DataParser(JSONObject jsonObject) {
    this.jsonObject = jsonObject;
  }

  public int checkStatus() {
    try {
      return jsonObject.getInt("status");
    } catch (JSONException e) {
      Log.e(TAG, "checkStatus: " + e.getMessage());
      return -1;
    }
  }

  public String parseMessage(int code) {
    try {
      if (code == 480) {
        return jsonObject.getString("message");
      } else if (code == 400) {
        JSONObject object = jsonObject.getJSONObject("data");
        return object.getString("message");
      } else {
        return "Server Error";
      }
    } catch (JSONException e) {
      Log.e(TAG, "parseMessage: " + e.getMessage());
    }
    return null;
  }

  public ArrayList<DataModel> parseDataList() {
    ArrayList<DataModel> scoreList = new ArrayList<>();
    try {
      JSONObject json_obj_images = jsonObject.getJSONObject("data");
      JSONArray array = json_obj_images.getJSONArray("images");

      for (int i = 0; i < array.length(); i++) {
        JSONObject object = array.getJSONObject(i);
        DataModel dataModel = new DataModel();
        dataModel.setUrl(object.getString("url"));
        dataModel.setTitle(object.getString("title"));
        dataModel.setHeight(object.getInt("height"));
        dataModel.setWidth(object.getInt("width"));
        dataModel.setUpload_at(object.getString("upload_at"));
        scoreList.add(dataModel);
      }
    } catch (JSONException e) {
      Log.e(TAG, "parseDataList: " + e.getMessage());
    }
    return scoreList;
  }
}
